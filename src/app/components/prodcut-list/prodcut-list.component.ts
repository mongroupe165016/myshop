import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/product';
@Component({
  selector: 'app-prodcut-list',
  templateUrl: './prodcut-list.component.html',
  styleUrls: ['./prodcut-list.component.css']
})
export class ProdcutListComponent implements  OnInit  {


products:Product[] = []

isDisplayModal : boolean = false;

modalProduct : Product | undefined
      

constructor(){}


ngOnInit(){

  this.products =[
    
    {
      _id: "124abc",
      name:"Robe imprimée rose",
        description:"Robe d'été",
        category:"Robe longue",
        imageUrl:[
          "./assets/images/products/robe1/0881337620_1_1_1.jpg",
          "./assets/images/products/robe1/0881337620_2_1_1.jpg",
          "./assets/images/products/robe1/0881337620_2_2_1.jpg",
          "./assets/images/products/robe1/0881337620_2_3_1.jpg"

        ],
        sold_price: 15.99,
        regular_price: 25.99,
        created_at: new Date(),
      
    },
    {
      _id:"135cba",
      name:"Robe imprimée blanche",
      description:"Robe d'été",
      category:"Robe longue",
      imageUrl:[
          "./assets/images/products/robe2/3653012330_1_1_1.jpg",
          "./assets/images/products/robe2/3653012330_2_1_1.jpg",
          "./assets/images/products/robe2/3653012330_2_3_1.jpg"
        ],
        sold_price: 18.99,
        regular_price: 38.99,
        created_at: new Date(),
      
    },
    {
      _id:"761oav",
      name:"Robe 3/4 rouge",
        description:"Robe pour l'élégance",
        category:"Robe ",
        imageUrl:[
          "./assets/images/products/robe3/2832715600_1_1_1.jpg",
          "./assets/images/products/robe3/2832715600_2_2_1.jpg",
          "./assets/images/products/robe3/2832715600_2_6_1.jpg",
          "./assets/images/products/robe3/2832715600_6_3_1.jpg"
        ],
        sold_price: 22.99,
        regular_price: 32.99,
        created_at: new Date(),
      
    },
    {
      _id:"961ne",
      
        name:"Robe imprimée blanche",
        description:"Robe d'été",
        category:"Robe longue",
        imageUrl:[
          "./assets/images/products/robe4/7521335747_1_1_1.jpg",
          "./assets/images/products/robe4/7521335747_2_1_1.jpg",
          "./assets/images/products/robe4/7521335747_2_3_1.jpg"

        ],
        sold_price: 19.99,
        regular_price: 29.99,
        created_at: new Date(),
      
    },
    {
      _id:"614jau",
      name:"Robe pour l'élégance",
      description:"Robe d'été",
      category:"Robe longue",
      imageUrl:[
          "./assets/images/products/robe5/8741041402_1_1_1.jpg",
          "./assets/images/products/robe5/8741041402_2_1_1.jpg",
          "./assets/images/products/robe5/8741041402_2_2_1.jpg",
          "./assets/images/products/robe5/8741041402_2_3_1.jpg",
          "./assets/images/products/robe5/8741041402_2_4_1.jpg"

        ],
        sold_price: 20.99,
        regular_price: 30.99,
        created_at: new Date(),
      
    },
    {
      _id:"518gqu",
      
        name:"Robe imprimée",
        description:"Robe d'été",
        category:"Robe longue",
        imageUrl:[
          "./assets/images/products/robe6/RUBY-2005-1.webp",
          "./assets/images/products/robe6/RUBY-2005-2.webp",
          "./assets/images/products/robe6/RUBY-2005-8.webp"
        ],
        sold_price: 32.99,
        regular_price: 47.99,
        created_at: new Date(),
      
    },
    {
      _id:"382lbr",
      name:"Robe d'été",
        description:"Robe pour l'élégance",
        category:"Robe longue",
        imageUrl:[
          "./assets/images/products/robe7/GAO-2071-8.jpg",
          "./assets/images/products/robe7/GAO-2071-v.webp",
          "./assets/images/products/robe7/GAO-2071-3.jpg"
        ],
        sold_price: 29.99,
        regular_price: 39.99,
        created_at: new Date(),
      
    },
    {
      _id:"592tdo",
      name:"Robe imprimée fleur",
      description:"Robe d'été",
      category:"Robe longue",
      imageUrl:[
          "./assets/images/products/robe8/GAO-2355-1.jpg",
          "./assets/images/products/robe8/GAO-2355-8.jpg",
          "./assets/images/products/robe8/GAO-2356-8.jpg"
        ],
        sold_price: 17.99,
        regular_price: 27.99,
        created_at: new Date(),
      
    },
    {

      _id:"618nau",
      name:"Robe imprimée fleur",
      description:"Robe d'été",
      category:"Robe longue",
      imageUrl:[
          "./assets/images/products/robe9/RUBY-2333-1.webp",
          "./assets/images/products/robe9/RUBY-2333-8.webp",
          "./assets/images/products/robe9/RUBY-2333-9.webp"
        ],
        sold_price: 27.99,
        regular_price: 37.99,
        created_at: new Date(),
      
    },
    {
      _id:"841doe",
      name:"Robe imprimée avec ceinture",
        description:"Robe imprimée ",
        category:"Robe longue",
        imageUrl:[
          "./assets/images/products/robe10/GEM-4440-1.webp",
          "./assets/images/products/robe10/GEM-4440-2.webp"
        ],
        sold_price: 37.99,
        regular_price: 47.99,
        created_at: new Date(),
      
    },
    {
      _id:"295abe",
      name:"Casual Button Elastic Slim Pants",
        description:"pantalon couleur rose",
        category:"pantalon",
        imageUrl:[
           "./assets/images/products/pantalon1/pan13.jpg",
          "./assets/images/products/pantalon1/pan11.jpg",
          "./assets/images/products/pantalon1/pan12.jpg",
          "./assets/images/products/pantalon1/pan14.jpg"
          
        ],
        sold_price: 27.99,
        regular_price: 37.99,
        created_at: new Date(),
      
    },
    {
      _id:"595aye",
      name:"Jean flare bleue ciel",
        description:"Jean Evasé",
        category:"Jean",
        imageUrl:[
         "./assets/images/products/jean1/jaen12.jpg",
         "/assets/images/products/jean1/jean10.jpg",
         "./assets/images/products/jean1/jean11.jpg"
        ],
        sold_price: 17.99,
        regular_price: 27.99,
        created_at: new Date(),
      
    },
    {
      _id:"498ike",
      name:"Jean flare bleu foncé",
        description:"Jean Evasé",
        category:"Jean",
        imageUrl:[
         "./assets/images/products/jean2/jean21.jpg",
         "./assets/images/products/jean2/jean22.jpg",
         "./assets/images/products/jean2/jean23.jpg",
         "./assets/images/products/jean2/jean24.jpg"
        ],
        sold_price: 14.99,
        regular_price: 24.99,
        created_at: new Date(),
      
    },
    {
      _id:"790ird",
      name:"Jean flare  blanc",
        description:"Jean Evasé",
        category:"Jean",
        imageUrl:[
         "./assets/images/products/jean3/jean30.jpg",
         "./assets/images/products/jean3/jean31.jpg",
         "./assets/images/products/jean3/jean32.jpg",
         "./assets/images/products/jean3/jean33.jpg",
         "./assets/images/products/jean3/jean34.jpg"
        ],
        sold_price: 14.99,
        regular_price: 24.99,
        created_at: new Date(),
      
    },
    {
      _id:"790ird",
      name:"Jean flare  blue foncé",
        description:"Jean Evasé",
        category:"Jean",
        imageUrl:[
         "./assets/images/products/jean4/jean40.jpg",
         "./assets/images/products/jean4/jean41.jpg",
         "./assets/images/products/jean4/jean42.jpg",
         "./assets/images/products/jean4/jean43.jpg",
         "./assets/images/products/jean4/jean45.jpg"
        ],
        sold_price: 13.99,
        regular_price: 23.99,
        created_at: new Date(),
      
    },

      
  ]
  
    
  }
  
  handleClickProductModal(prodcut:Product){
    if(prodcut){
     
      this.isDisplayModal = true
      this.modalProduct = prodcut
    }

  }

handleDeleteProduct(prodcut:Product){
  //console.log("handleDeleteProduct:", prodcut)

  this.products = this.products.filter((p)=> p._id !== prodcut._id)
 // console.log(this.products);

}

handleCloseModal(){
  this.isDisplayModal = false
  this.modalProduct = undefined
}




}
