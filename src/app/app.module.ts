import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ContainerComponent } from './components/container/container.component';
import { FooterComponent } from './components/footer/footer.component';
import { ProdcutComponent } from './components/prodcut/prodcut.component';
import { ProdcutListComponent } from './components/prodcut-list/prodcut-list.component';
import { ProdcutItemComponent } from './components/prodcut-item/prodcut-item.component';
import { ModalProductViewComponent } from './components/modal-product-view/modal-product-view.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContainerComponent,
    FooterComponent,
    ProdcutComponent,
    ProdcutListComponent,
    ProdcutItemComponent,
    ModalProductViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
